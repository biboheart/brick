# bh-brick

#### 项目介绍
项目中常用功能整合，时间工具、bean、图片工具、Json工具、Pinyin工具、二维码工具等。使用中有什么建议或意见请在码云评论反馈。

#### 安装教程

```
<dependency>
  <groupId>com.biboheart</groupId>
  <artifactId>bh-brick</artifactId>
  <version>{version}</version>
</dependency>
```

#### 参与贡献

1. 本项目由碧波之心开发提交。
2. 项目依赖:

```
<dependency>
	<groupId>com.fasterxml.jackson.core</groupId>
	<artifactId>jackson-databind</artifactId>
	<version>2.8.7</version>
</dependency>

<dependency>
	<groupId>com.belerweb</groupId>
	<artifactId>pinyin4j</artifactId>
	<version>2.5.1</version>
</dependency>

<dependency>
	<groupId>com.google.zxing</groupId>
	<artifactId>core</artifactId>
	<version>3.3.2</version>
</dependency>

<dependency>
	<groupId>com.google.zxing</groupId>
	<artifactId>javase</artifactId>
	<version>3.3.2</version>
</dependency>
```
#### 接口说明
1. BeanUtils

函数：copyMapToBeanProperties
功能：map key和value设置到bean中
返回：List<String> (被修改的属性列表)
参数：

|名称|类型|必须|描述|
| --- | --- | --- | --- |
|source|map|是|Map对象，数据源|
|target|Object|是|bean 对象，设置目标|
|scope|List<String>|否|属性范围，只拷贝列表指定的属性，如果值为空，则取所有属性|
|exclude|List<String>|否|排除属性，不拷贝列表指定的属性|

函数：beanToMap
功能：把bean对象转换成map对象
返回：List<String> (被修改的属性列表)
参数：

|名称|类型|必须|描述|
| --- | --- | --- | --- |
|source|Object|是|bean对象，数据源|
|target|Map<String, Object>|是|Map 对象，目标|
|scope|List<String>|否|属性范围，只拷贝列表指定的属性，如果值为空，则取所有属性|
|exclude|List<String>|否|排除属性，不拷贝列表指定的属性|

函数：copyMapToMap
功能：两个map之间数据拷贝
返回：List<String> (被修改的属性列表)
参数：

|名称|类型|必须|描述|
| --- | --- | --- | --- |
|source|Map<String, Object>|是|bean对象，数据源|
|target|Map<String, Object>|是|Map 对象，目标|
|scope|List<String>|否|属性范围，只拷贝列表指定的属性，如果值为空，则取所有属性|
|exclude|List<String>|否|排除属性，不拷贝列表指定的属性|

函数：copy
功能：两个bean对象之间数据拷贝
返回：List<String> (被修改的属性列表)
参数：

|名称|类型|必须|描述|
| --- | --- | --- | --- |
|source|Object|是|bean对象，数据源|
|target|Object|是|Map 对象，目标|
|scope|List<String>|否|属性范围，只拷贝列表指定的属性，如果值为空，则取所有属性|
|exclude|List<String>|否|排除属性，不拷贝列表指定的属性|

函数：defaultsDeep
功能：从source覆盖target中为null的项
返回：List<String> (被修改的属性列表)
参数：

|名称|类型|必须|描述|
| --- | --- | --- | --- |
|source|Object|是|bean对象，数据源|
|target|Object|是|Map 对象，目标|
|scope|List<String>|否|属性范围，只拷贝列表指定的属性，如果值为空，则取所有属性|
|exclude|List<String>|否|排除属性，不拷贝列表指定的属性|

函数：compareObjIntegerValue
功能：将Integer|Long属性值为NULL的设为0
返回：无
参数：

|名称|类型|必须|描述|
| --- | --- | --- | --- |
|source|Object|是|bean对象，数据源|
|scope|List<String>|否|属性范围，只转换列表指定的属性，如果值为空，则取所有属性|
|exclude|List<String>|否|排除属性，不转换列表指定的属性|

函数：compareObjCommaString2BraceString
功能：将scope指点的属性的值从22,33,55转换成(22)(33)(55)
返回：无
参数：

|名称|类型|必须|描述|
| --- | --- | --- | --- |
|source|Object|是|bean对象，数据源|
|scope|List<String>|是|属性范围，转换列表指定的属性|

函数：compareObjDiffParams
功能：比较两个对象值不同的属性，要求两个对象是同一个类的对象
返回：List<String> (不同值的属性列表)
参数：

|名称|类型|必须|描述|
| --- | --- | --- | --- |
|source|Object|是|bean对象，数据源|
|target|Object|是|Map 对象，目标|
|scope|List<String>|否|属性范围，只比较列表指定的属性，如果值为空，则取所有属性|
|exclude|List<String>|否|排除属性，不比较列表指定的属性|

函数：compareObjDiffPropertys
功能：比较两个对象不同名的属性
返回：List<String> (不同名的属性列表)
参数：

|名称|类型|必须|描述|
| --- | --- | --- | --- |
|source|Object|是|bean对象，数据源|
|target|Object|是|Map 对象，目标|
|scope|List<String>|否|属性范围，只比较列表指定的属性，如果值为空，则取所有属性|
|exclude|List<String>|否|排除属性，不比较列表指定的属性|

函数：compareObj
功能：比较两个BEAN或MAP对象的值是否相等
返回：boolean (true: 相等；false：不相等)
参数：

|名称|类型|必须|描述|
| --- | --- | --- | --- |
|source|Object|是|bean对象，数据源|
|target|Object|是|Map 对象，目标|
