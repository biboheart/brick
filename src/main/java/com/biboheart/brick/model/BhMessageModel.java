package com.biboheart.brick.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BhMessageModel {
	private String name; // 消息名称
	private String topic; // 消息主题，以{系统名}-{资源名称}-{操作类型}命名，如person-org-add
	private String source; // 来源，是由哪个服务发出的消息
	private String sender; // 发送者，可以不指定
	private String receiver; // 接收者，发送的目标，可以不指定
	private String code; // 消息标识
	private Long time; // 消息时间戳
	private Object message; // 消息主体，json
}
