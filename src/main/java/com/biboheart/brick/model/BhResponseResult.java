package com.biboheart.brick.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BhResponseResult<T> {
	public static final BhResponseResult<?> EMPTY = new BhResponseResult<>();
	
	private int code; // 结果码，对应结果码表
	private String message; // 结果描述
	private T result; // 结果

	public static <T> BhResponseResult<T> success(T result) {
		return new BhResponseResult<>(0, "success", result);
	}
}
