package com.biboheart.brick.model;

import com.biboheart.brick.exception.BhException;
import com.biboheart.brick.utils.CheckUtils;
import com.biboheart.brick.utils.JsonUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BhRpcResult implements Serializable {
	private static final long serialVersionUID = -2758499680873677496L;
	
	private int code; // 结果码，对应结果码表
	private String message; // 结果描述
	private Object result; // 实际结果JSON字符串

	/**
	 * 从相同结构的json串中取出结果
	 * @param input 待解析的数据
	 * @return 解析成功返回rpc结果
	 * @throws BhException 解析异常
	 */
	public static Object getResult(Object input) throws BhException {
		if (null == input) {
			return null;
		}
		if (input instanceof BhRpcResult) {
			return ((BhRpcResult) input).getResult();
		}
		String str = String.valueOf(input);
		if (CheckUtils.isEmpty(str)) {
			return null;
		}
		BhRpcResult rpcResult;
		try {
			rpcResult = (BhRpcResult) JsonUtils.json2obj(str, BhRpcResult.class);
		} catch (Exception e) {
			rpcResult = new BhRpcResult(0, "success", String.valueOf(input));
		}
		if (0 != rpcResult.getCode()) {
			throw new BhException(rpcResult.getMessage());
		}
		return rpcResult.getResult();
	}

	/**
	 * 从相同结构的json串中取出结果
	 * @param input 待解析的数据
	 * @param valueType 接收结果类型
	 * @param <T> 泛型结果类型
	 * @return 解析成功返回rpc结果
	 * @throws BhException 解析异常
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getResult(Object input, Class<T> valueType) throws BhException {
		Object result = getResult(input);
		if (valueType.isInstance(result)) {
			return valueType.cast(result);
		}
		String str = (result instanceof String) ? (String) result : JsonUtils.obj2json(result);
		if (null == str || str.isEmpty()) {
			return null;
		}
		if (String.class.equals(valueType)) {
			return (T) str;
		}
		return (T) JsonUtils.json2obj(str, valueType);
	}
}
