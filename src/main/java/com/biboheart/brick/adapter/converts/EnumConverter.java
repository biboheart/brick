package com.biboheart.brick.adapter.converts;

import com.biboheart.brick.adapter.Converter;
import com.biboheart.brick.adapter.TypeConverter;
import com.biboheart.brick.utils.CheckUtils;
import com.biboheart.brick.utils.ListUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 枚举转换器
 * 例如：
 * 枚举定义：{"def":"默认","1":"男","2":"女"}
 * 输入值：1,2
 * 输出值：男,女
 */
public class EnumConverter implements Converter {
    @SuppressWarnings("unchecked")
    @Override
    public <T> T convert(Object input, String auxiliary, Class<T> valueType) {
        if (CheckUtils.isEmpty(auxiliary)) {
            return null;
        }
        Map<String, Object> enumMap = TypeConverter.convert(auxiliary, Map.class);
        String key = (null == input || "".equals(input)) ? "def" : String.valueOf(input);
        List<String> keyList = ListUtils.string2ListString(key, ",");
        Set<String> vals = new HashSet<>();
        for (String keyItem : keyList) {
            if (!enumMap.containsKey(keyItem)) {
                keyItem = "def";
            }
            Object temp = enumMap.get(keyItem);
            if (null == temp) {
                continue;
            }
            vals.add(String.valueOf(temp));
        }
        if (CheckUtils.isEmpty(vals)) {
            input = null;
        } else {
            input = String.join(",", vals);
        }
        return TypeConverter.convert(input, valueType);
    }
}
