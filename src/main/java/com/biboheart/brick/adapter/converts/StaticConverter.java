package com.biboheart.brick.adapter.converts;

import com.biboheart.brick.adapter.Converter;
import com.biboheart.brick.adapter.TypeConverter;

/**
 * 静态转换器
 * 当输入值为null时，使用辅助值进行转换，否则使用输入值进行转换
 * 例如：
 * 输入值为null，辅助值为"15"，转换为"15"
 * 输入值为"7"，辅助值为"15"，转换为"7"
 */
public class StaticConverter implements Converter {
    @Override
    public <T> T convert(Object input, String auxiliary, Class<T> valueType) {
        if (null == input && (null == auxiliary || auxiliary.isEmpty())) {
            return null;
        }
        if (null == input) {
            input = auxiliary;
        }
        return TypeConverter.convert(input, valueType);
    }
}
