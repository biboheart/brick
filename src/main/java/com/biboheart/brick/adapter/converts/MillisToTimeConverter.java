package com.biboheart.brick.adapter.converts;

import com.biboheart.brick.adapter.Converter;
import com.biboheart.brick.adapter.TypeConverter;
import com.biboheart.brick.utils.TimeUtils;

/**
 * 毫秒数转时间转换器
 * 例如：
 * 输入为1672531200000，辅助参数为"yyyy-MM-dd HH:mm:ss"，转换为"2023-01-01 00:00:00"
 */
public class MillisToTimeConverter implements Converter {
    @Override
    public <T> T convert(Object input, String auxiliary, Class<T> valueType) {
        Long val = TypeConverter.convert(input, Long.class);
        if (null == val) {
            return null;
        }
        return TypeConverter.convert(TimeUtils.formatDateWithMilis(auxiliary, val), valueType);
    }
}
