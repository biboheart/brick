package com.biboheart.brick.adapter.converts;

import com.biboheart.brick.adapter.Converter;
import com.biboheart.brick.adapter.TypeConverter;

/**
 * 写入二进位位转换器
 * 写入auxiliary位上的二进制位
 * 例如：
 * 10进制数10，转换为2进制数为1010，写入第2位上的二进制位为1，转换为10进制数为14
 */
public class BinaryDigitInConverter implements Converter {
    @Override
    public <T> T convert(Object input, String auxiliary, Class<T> valueType) {
        Long inputNumber = TypeConverter.convertToLong(input);
        if (null == inputNumber) {
            inputNumber = 0L;
        }
        int digit = TypeConverter.convertToInteger(auxiliary);
        if (digit <= 0) {
            return TypeConverter.convert(inputNumber, valueType);
        }
        digit = digit - 1;
        return TypeConverter.convert(inputNumber | (1L << digit), valueType);
    }
}
