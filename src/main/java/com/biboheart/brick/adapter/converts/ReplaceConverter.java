package com.biboheart.brick.adapter.converts;

import com.biboheart.brick.adapter.Converter;
import com.biboheart.brick.adapter.TypeConverter;

/**
 * 替换转换器
 * 例如：
 * 输入为"15"，辅助参数为"年龄{[value]}岁"，转换为"年龄15岁"
 */
public class ReplaceConverter implements Converter {
    @Override
    public <T> T convert(Object input, String auxiliary, Class<T> valueType) {
        if (null == auxiliary || auxiliary.isEmpty()) {
            return TypeConverter.convert(input, valueType);
        }
        String val = TypeConverter.convert(input, String.class);
        return TypeConverter.convert(auxiliary.replaceAll("\\{\\[value]}", val), valueType);
    }
}
