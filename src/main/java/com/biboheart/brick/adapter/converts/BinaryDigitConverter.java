package com.biboheart.brick.adapter.converts;

import com.biboheart.brick.adapter.Converter;
import com.biboheart.brick.adapter.TypeConverter;

/**
 * 二进制位转换器
 * 提取auxiliary位上的二进制位
 * 例如：
 * 10进制数10，转换为2进制数为1010，提取第2位上的二进制位为1，转换为10进制数为1
 */
public class BinaryDigitConverter implements Converter {
    @Override
    public <T> T convert(Object input, String auxiliary, Class<T> valueType) {
        Long inputNumber = TypeConverter.convertToLong(input);
        if (null == inputNumber || 0 == inputNumber) {
            return TypeConverter.convert(0, valueType);
        }
        int digit = TypeConverter.convertToInteger(auxiliary);
        if (digit <= 0) {
            return TypeConverter.convert(0, valueType);
        }
        digit = digit - 1;
        return TypeConverter.convert((inputNumber >> digit) & 1, valueType);
    }
}
