package com.biboheart.brick.adapter.converts;

import com.biboheart.brick.adapter.Converter;
import com.biboheart.brick.adapter.TypeConverter;
import com.biboheart.brick.utils.CheckUtils;
import com.biboheart.brick.utils.TimeUtils;

/**
 * 时间转换为毫秒数转换器，辅助参数为时间格式，例如：yyyy-MM-dd HH:mm:ss
 */
public class TimeToMillisConverter implements Converter {
    @Override
    public <T> T convert(Object input, String auxiliary, Class<T> valueType) {
        String val = TypeConverter.convert(input, String.class);
        if (CheckUtils.isEmpty(val)) {
            return null;
        }
        if (null != auxiliary && auxiliary.contains("/") && val.contains("-")) {
            auxiliary = auxiliary.replace('/', '-');
        }
        return TypeConverter.convert(TimeUtils.getDateMillis(val, auxiliary), valueType);
    }
}
