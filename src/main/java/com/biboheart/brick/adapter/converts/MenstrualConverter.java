package com.biboheart.brick.adapter.converts;


import com.biboheart.brick.adapter.Converter;
import com.biboheart.brick.adapter.TypeConverter;

/**
 * 经期转换器
 * 例如：
 * 输入为"15|7|30|35"，转换为"初潮年龄15岁经期7天周期30天末次月经35"
 */
public class MenstrualConverter implements Converter {
    @Override
    public <T> T convert(Object input, String auxiliary, Class<T> valueType) {
        if (null == input && (null == auxiliary || auxiliary.isEmpty())) {
            return null;
        }
        if (null == input) {
            input = auxiliary;
        }
        String val = TypeConverter.convert(input, String.class);
        String[] valArr = val.split("\\|");
        if (valArr.length == 0) {
            return TypeConverter.convert(input, valueType);
        }
        StringBuilder sb = new StringBuilder();
        if (!valArr[0].isEmpty()) {
            sb.append("初潮年龄").append(valArr[0]).append("岁");
        }
        if (valArr.length > 1 && !valArr[1].isEmpty()) {
            sb.append("经期").append(valArr[1]).append("天");
        }
        if (valArr.length > 2 && !valArr[2].isEmpty()) {
            sb.append("周期").append(valArr[2]).append("天");
        }
        if (valArr.length > 3 && !valArr[3].isEmpty()) {
            sb.append("末次月经").append(valArr[3]);
        }
        if (valArr.length > 4 && !valArr[4].isEmpty()) {
            sb.append(valArr[4]);
        }
        return TypeConverter.convert(sb.toString(), valueType);
    }
}
