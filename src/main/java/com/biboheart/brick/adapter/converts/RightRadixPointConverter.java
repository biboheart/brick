package com.biboheart.brick.adapter.converts;

import com.biboheart.brick.adapter.Converter;
import com.biboheart.brick.adapter.TypeConverter;
import com.biboheart.brick.utils.ExchangeUtils;

import java.util.Map;

/**
 * 右移小数点转换器
 * 例如：
 * 输入为55.3，辅助参数为{"move": 4, "fractional": 2}，转换为553000.00
 */
public class RightRadixPointConverter implements Converter {
    @SuppressWarnings("unchecked")
    @Override
    public <T> T convert(Object input, String auxiliary, Class<T> valueType) {
        if (null == input) {
            return null;
        }
        if (null == auxiliary) {
            return TypeConverter.convert(input, valueType);
        }
        String val = TypeConverter.convert(input, String.class);
        Map<String, Object> params = TypeConverter.convert(auxiliary, Map.class);
        Integer moveLen = TypeConverter.convert(params.get("move"), Integer.class);
        Integer fractional = TypeConverter.convert(params.get("fractional"), Integer.class);
        Double dval = ExchangeUtils.moveRadixPoint(val, moveLen, fractional);
        if (null == dval) {
            return null;
        }
        return TypeConverter.convert(dval.longValue(), valueType);
    }
}
