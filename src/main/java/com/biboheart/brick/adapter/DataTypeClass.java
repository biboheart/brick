package com.biboheart.brick.adapter;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;

@Getter
@RequiredArgsConstructor
public enum DataTypeClass {
    TYPE_LIST("list", List.class),
    TYPE_MAP("map", Map.class),
    TYPE_STRING("string", String.class),
    TYPE_LONG("long", Long.class),
    TYPE_INT("int", Integer.class),
    TYPE_DOUBLE("double", Double.class),
    TYPE_OBJECT("object", Object.class),
    TYPE_BOOLEAN("boolean", Boolean.class),
    ;
    private final String code;
    private final Class<?> cls;

    public String code() {
        return code;
    }
    public Class<?> desc() {
        return cls;
    }

    public static DataTypeClass getClass(String code) {
        if (null == code || code.isEmpty()) return DataTypeClass.TYPE_OBJECT;
        DataTypeClass[] types = DataTypeClass.values();
        for (DataTypeClass type : types) {
            if (type.code().equalsIgnoreCase(code)) {
                return type;
            }
        }
        return DataTypeClass.TYPE_OBJECT;
    }
}
