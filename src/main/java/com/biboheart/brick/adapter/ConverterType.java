package com.biboheart.brick.adapter;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ConverterType {
    CONVERTER_STATIC("static", "静态"),
    CONVERTER_REPLACE("replace", "替换"),
    CONVERTER_FILL("fill", "填充"),
    CONVERTER_ENUM("enum", "枚举"),
    CONVERTER_SPLIT("split", "枚举"),
    CONVERTER_RIGHT_RADIX_POINT("rightRadixPoint", "小数点右移"),
    CONVERTER_LEFT_RADIX_POINT("leftRadixPoint", "小数点左移"),
    CONVERTER_TIME_MILLIS("timeToMillis", "时间转时间戳"),
    CONVERTER_MILLIS_TIME("millisToTime", "时间戳转时间"),
    CONVERTER_BINARY_DIGIT("binaryDigit", "二进位"),
    CONVERTER_BINARY_DIGIT_IN("binaryDigitIn", "写入二进位"),
    CONVERTER_MENSTRUAL("menstrual", "月经史"),
    ;

    private final String code;
    private final String desc;

    public static ConverterType getConverterType(String name) {
        ConverterType[] converterTypes = ConverterType.values();
        for (ConverterType converterType : converterTypes) {
            if (converterType.getCode().equals(name)) {
                return converterType;
            }
        }
        return ConverterType.CONVERTER_STATIC;
    }
}
