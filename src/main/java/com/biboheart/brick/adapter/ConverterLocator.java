package com.biboheart.brick.adapter;

import com.biboheart.brick.adapter.converts.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 转换器定位器
 */
public class ConverterLocator {
    private final Map<ConverterType, Converter> converterMap = new HashMap<>();

    public ConverterLocator() {
        this.init();
    }

    public Converter get(ConverterType type) {
        return converterMap.get(type);
    }

    public Converter get(String type) {
        return converterMap.get(ConverterType.getConverterType(type));
    }

    private void init() {
        converterMap.put(ConverterType.CONVERTER_STATIC, new StaticConverter());
        converterMap.put(ConverterType.CONVERTER_REPLACE, new ReplaceConverter());
        converterMap.put(ConverterType.CONVERTER_FILL, new FillConverter());
        converterMap.put(ConverterType.CONVERTER_ENUM, new EnumConverter());
        converterMap.put(ConverterType.CONVERTER_SPLIT, new SplitConverter());
        converterMap.put(ConverterType.CONVERTER_RIGHT_RADIX_POINT, new RightRadixPointConverter());
        converterMap.put(ConverterType.CONVERTER_LEFT_RADIX_POINT, new LeftRadixPointConverter());
        converterMap.put(ConverterType.CONVERTER_TIME_MILLIS, new TimeToMillisConverter());
        converterMap.put(ConverterType.CONVERTER_MILLIS_TIME, new MillisToTimeConverter());
        converterMap.put(ConverterType.CONVERTER_BINARY_DIGIT, new BinaryDigitConverter());
        converterMap.put(ConverterType.CONVERTER_BINARY_DIGIT_IN, new BinaryDigitInConverter());
        converterMap.put(ConverterType.CONVERTER_MENSTRUAL, new MenstrualConverter());
    }
}
