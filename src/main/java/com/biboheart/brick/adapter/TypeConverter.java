package com.biboheart.brick.adapter;

import com.biboheart.brick.utils.JsonUtils;

public class TypeConverter {
    /**
     * 数据类型转换
     * @param input 输入值
     * @param valueType 目标类型
     * @return 转换后的值
     * @param <T> 目标类型
     */
    public static <T> T convert(Object input, Class<T> valueType) {
        if (null == input) {
            return null;
        }
        if (valueType.isInstance(input)) {
            return valueType.cast(input);
        }
        if (Long.class.isAssignableFrom(valueType)) {
            return valueType.cast(convertToLong(input));
        } else if (Integer.class.isAssignableFrom(valueType)) {
            return valueType.cast(convertToInteger(input));
        } else if (Double.class.isAssignableFrom(valueType)) {
            return valueType.cast(convertToDouble(input));
        } else if (String.class.isAssignableFrom(valueType)) {
            return valueType.cast(convertToString(input));
        } else if (Boolean.class.isAssignableFrom(valueType)) {
            return valueType.cast(convertToBoolean(input));
        } else if (input instanceof String) {
            return valueType.cast(JsonUtils.json2obj(String.valueOf(input), valueType));
        } else {
            return null;
        }
    }

    /**
     * 数据类型转换, 将输入值转换为布尔值
     * @param input 输入值
     * @return 转换后的值
     */
    public static Boolean convertToBoolean(Object input) {
        if (null == input) {
            return false;
        }
        if (input instanceof Boolean) {
            return (Boolean) input;
        }
        if (input instanceof String) {
            try {
                return Boolean.parseBoolean(String.valueOf(input));
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }

    /**
     * 数据类型转换, 将输入值转换为整数
     * @param input 输入值
     * @return 转换后的值
     */
    public static Integer convertToInteger(Object input) {
        if (null == input) {
            return 0;
        }
        if (!(input instanceof Long) && !(input instanceof Integer) && !(input instanceof String)) {
            return 0;
        }
        if (input instanceof Integer) {
            return (Integer) input;
        } else {
            try {
                return Integer.parseInt(String.valueOf(input));
            } catch (NumberFormatException e) {
                return 0;
            }
        }
    }

    /**
     * 数据类型转换, 将输入值转换为浮点数
     * @param input 输入值
     * @return 转换后的值
     */
    public static Double convertToDouble(Object input) {
        if (null == input) {
            return null;
        }
        if (!(input instanceof Number) && !(input instanceof String)) {
            return null;
        }
        if (input instanceof Number) {
            return ((Number) input).doubleValue();
        } else {
            try {
                return Double.parseDouble(String.valueOf(input));
            } catch (NumberFormatException e) {
                return null;
            }
        }
    }

    /**
     * 数据类型转换, 将输入值转换为长整数
     * @param input 输入值
     * @return 转换后的值
     */
    public static Long convertToLong(Object input) {
        if (null == input) {
            return null;
        }
        if (!(input instanceof Long) && !(input instanceof Integer) && !(input instanceof String)) {
            return null;
        }
        if (input instanceof Long) {
            return (Long) input;
        } else {
            try {
                return Long.parseLong(String.valueOf(input));
            } catch (NumberFormatException e) {
                return null;
            }
        }
    }

    /**
     * 数据类型转换, 将输入值转换为字符串
     * @param input 输入值
     * @return 转换后的值
     */
    public static String convertToString(Object input) {
        if (null == input) {
            return null;
        }
        if (!(input instanceof Long) && !(input instanceof Integer) && !(input instanceof Double) && !(input instanceof String)) {
            return null;
        }
        if (input instanceof String) {
            return (String) input;
        } else {
            try {
                return String.valueOf(input);
            } catch (Exception e) {
                return null;
            }
        }
    }
}
