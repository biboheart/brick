package com.biboheart.brick.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PrimaryTransverter {
	/**
	 * 把(22)(33)(55)或者22,33,55形成List(Long)
	 * 
	 * @param ids
	 *            ID字符串
	 * @return 数值列表
	 */
	@Deprecated
	public static List<Long> idsStr2LongList(String ids) {
		if (null == ids) {
			return null;
		}
		ids = ids.trim();
		if (ids.isEmpty()) {
			return null;
		}
		List<Long> list = new ArrayList<>();
		if (0 == ids.indexOf("(")) {
			Pattern pattern = Pattern.compile("(\\d+)");
			Matcher m = pattern.matcher(ids);
			while (m.find()) {
				try {
					list.add(Long.parseLong(m.group()));
				} catch (NumberFormatException e) {
					continue;
				}
			}
			return list;
		} else {
			String[] strArr = ids.split(",");
			for (int i = 0; i < strArr.length; i++) {
				try {
					list.add(Long.parseLong(strArr[i]));
				} catch (NumberFormatException e) {
					continue;
				}
			}
			return list;
		}
	}

	/**
	 * 把22,33,55转换成Long数组
	 * 
	 * @param str
	 *            id字符串
	 * @return ID数组
	 */
	public static Long[] commaStr2LongArray(String str) {
		if (null == str || "".equals(str)) {
			return null;
		}
		String[] strArr = str.split(",");
		if (0 >= strArr.length) {
			return null;
		} else {
			Long[] intArr = new Long[strArr.length];
			for (int i = 0; i < strArr.length; i++) {
				intArr[i] = Long.valueOf(strArr[i]);
			}
			return intArr;
		}
	}

	/**
	 * 把(22)(33)(55)或者22,33,55形成List
	 * 
	 * @param ids
	 *            id字符串
	 * @return 数值列表
	 */
	public static List<Integer> idsStr2List(String ids) {
		if (null == ids) {
			return null;
		}
		ids = ids.trim();
		if (ids.equals("")) {
			return null;
		}
		List<Integer> list = new ArrayList<Integer>();
		if (0 == ids.indexOf("(")) {
			Pattern pattern = Pattern.compile("(\\d+)");
			Matcher m = pattern.matcher(ids);
			while (m.find()) {
				try {
					list.add(Integer.parseInt(m.group()));
				} catch (NumberFormatException e) {
					continue;
				}
			}
			return list;
		} else {
			String[] strArr = ids.split(",");
			for (int i = 0; i < strArr.length; i++) {
				try {
					list.add(Integer.parseInt(strArr[i]));
				} catch (NumberFormatException e) {
					continue;
				}
			}
			return list;
		}
	}

	/**
	 * 把22,33,55转换成Integer数组
	 * 
	 * @param str
	 *            id字符串
	 * @return ID数组
	 */
	public static Integer[] commaStr2Array(String str) {
		if (null == str || "".equals(str)) {
			return null;
		}
		String[] strArr = str.split(",");
		if (0 >= strArr.length) {
			return null;
		} else {
			Integer[] intArr = new Integer[strArr.length];
			for (int i = 0; i < strArr.length; i++) {
				intArr[i] = Integer.valueOf(strArr[i]);
			}
			return intArr;
		}
	}

	/**
	 * 把(22)(33)(55)转换成22,33,55
	 * 
	 * @param str
	 *            ID字符串
	 * @return 用“,”分隔的ID字符串
	 */
	public static String braceString2CommaString(String str) {
		if (null == str || "".equals(str)) {
			return "";
		}
		Pattern pattern = Pattern.compile("(\\d+)");
		Matcher m = pattern.matcher(str);
		String value = "";
		while (m.find()) {
			value += Integer.parseInt(m.group()) + ",";
		}
		if (!"".equals(value)) {
			value = value.substring(0, value.length() - 1);
		}
		return value;
	}

	/**
	 * 把22,33,55转换成(22)(33)(55)
	 * 
	 * @param str
	 *            逗号分隔的ID字符串
	 * @return 括号分隔的ID字符串
	 */
	public static String commaString2BraceString(String str) {
		if (0 == str.indexOf("(")) {
			return str;
		}
		if (null == str || "".equals(str)) {
			return "";
		}
		str = str.replace(",", ")(");
		str = "(" + str + ")";
		return str;
	}

	/**
	 * 把list转换成22,33,55
	 * 
	 * @param vs
	 *            ID列表
	 * @return 逗号分隔的ID字符串
	 */
	public static String list2String(List<Integer> vs) {
		if (null == vs || vs.isEmpty()) {
			return null;
		}
		String valueStr = "";
		for (Integer v : vs) {
			if (null != v && 0 <= v) {
				valueStr += v + ",";
			}
		}
		if (!"".equals(valueStr)) {
			valueStr = valueStr.substring(0, valueStr.length() - 1);
		}
		return valueStr;
	}

	/**
	 * 把list转换成22,33,55
	 * 
	 * @param vs
	 *            ID列表
	 * @return 逗号分隔的ID字符串
	 */
	public static String listLong2String(List<Long> vs) {
		if (null == vs || vs.isEmpty()) {
			return null;
		}
		String valueStr = "";
		for (Long v : vs) {
			if (null != v && 0 <= v) {
				valueStr += v + ",";
			}
		}
		if (!"".equals(valueStr)) {
			valueStr = valueStr.substring(0, valueStr.length() - 1);
		}
		return valueStr;
	}

	/**
	 * 把list转换成(22)(33)(55)
	 * 
	 * @param vs
	 *            ID列表
	 * @return 括号分隔的ID字符串
	 */
	public static String list2BraceString(List<Integer> vs) {
		if (null == vs || vs.isEmpty()) {
			return null;
		}
		String valueStr = "";
		for (Integer v : vs) {
			if (null != v && 0 <= v) {
				valueStr += "(" + v + ")";
			}
		}
		return valueStr;
	}

	/**
	 * 把list转换成(22)(33)(55)
	 * 
	 * @param vs
	 *            ID列表
	 * @return 用括号分隔的ID字符串
	 */
	public static String listLong2BraceString(List<Long> vs) {
		if (null == vs || vs.isEmpty()) {
			return null;
		}
		String valueStr = "";
		for (Long v : vs) {
			if (null != v && 0 <= v) {
				valueStr += "(" + v + ")";
			}
		}
		return valueStr;
	}
}
