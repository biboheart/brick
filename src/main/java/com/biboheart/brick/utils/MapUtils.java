package com.biboheart.brick.utils;

import java.util.Map;

public class MapUtils {
	/**
	 * 从MAP对象中取值
	 * @param data 数据MAP对象
	 * @param key 取值的键
	 * @param valueType 值类型
	 * @param def 默认值，如果值为null时，返回的值
	 */
	public static <K, V, T> T getValue(Map<K, V> data, K key, T def, Class<T> valueType) {
		V value;
		if(null == data || null == key) {
			value = null;
		} else {
			value = data.get(key);
		}
		if (null == value) {
			return def;
		}
		if (valueType.isInstance(value)) {
			return valueType.cast(value);
		}
		T res;
		String str = (value instanceof String) ? (String) value : String.valueOf(value);
		if (Long.class.isAssignableFrom(valueType)) {
			try {
				res = valueType.cast(Long.parseLong(str));
			} catch (NumberFormatException e) {
				res = null;
			}
		} else if (Integer.class.isAssignableFrom(valueType)) {
			try {
				res = valueType.cast(Integer.parseInt(str));
			} catch (NumberFormatException e) {
				res = null;
			}
		} else if (Double.class.isAssignableFrom(valueType)) {
			try {
				res = valueType.cast(Double.parseDouble(str));
			} catch (NumberFormatException e) {
				res = null;
			}
		} else if (Float.class.isAssignableFrom(valueType)) {
			try {
				res = valueType.cast(Float.parseFloat(str));
			} catch (NumberFormatException e) {
				res = null;
			}
		} else if (String.class.isAssignableFrom(valueType)) {
			try {
				res = valueType.cast(str);
			} catch (NumberFormatException e) {
				res = null;
			}
		} else {
			res = null;
		}
		return null == res ? def : res;
	}

	public static <K, V, T> T getValue(Map<K, V> data, K key, Class<T> valueType) {
		return getValue(data, key, null, valueType);
	}

	/**
	 * map对象中取int的值
	 * 
	 * @param source
	 *            map对象
	 * @param key
	 *            名称
	 * @return 值 ，如果不能转换为数值返回null
	 */
	public static <K, V> Integer getIntValue(Map<K, V> source, K key) {
		if(null == source) {
			return null;
		}
		Object valObj = source.get(key);
		if(null == valObj) {
			return null;
		}
		if(!(valObj instanceof Long) && !(valObj instanceof Integer) && !(valObj instanceof String)) {
			return null;
		}
		Integer val;
		try {
			val = Integer.parseInt(String.valueOf(valObj));
		} catch (NumberFormatException e) {
			val = null;
		}
		return val;
	}

	/**
	 * map对象中取long的值
	 * 
	 * @param source
	 *            map对象
	 * @param key
	 *            名称
	 * @return 值 ，如果不能转换为数值返回null
	 */
	public static <K, V> Long getLongValue(Map<K, V> source, K key) {
		if(null == source) {
			return null;
		}
		Object valObj = source.get(key);
		if(null == valObj) {
			return null;
		}
		if(!(valObj instanceof Long) && !(valObj instanceof Integer) && !(valObj instanceof String)) {
			return null;
		}
		Long val;
		try {
			val = Long.valueOf(String.valueOf(valObj));
		} catch (NumberFormatException e) {
			val = null;
		}
		return val;
	}

	/**
	 * map对象中取String的值
	 * 
	 * @param source
	 *            map对象
	 * @param key
	 *            名称
	 * @return 值，如果转换失败返回null
	 */
	public static <K, V> String getStringValue(Map<K, V> source, K key) {
		if(null == source) {
			return null;
		}
		Object valObj = source.get(key);
		if(null == valObj) {
			return null;
		}
		String val;
		if("String".equals(valObj.getClass().getSimpleName())) {
			val = (String)valObj;
		} else {
			val = String.valueOf(valObj);
		}
		if("null".equals(val)) {
			val = null;
		}
		return val;
	}
}
