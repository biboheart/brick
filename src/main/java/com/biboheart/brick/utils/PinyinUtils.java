package com.biboheart.brick.utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

public class PinyinUtils {
	private final static HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
	static {
		format.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		format.setVCharType(HanyuPinyinVCharType.WITH_V);
	}

	/**
	 * 字符串转拼音
	 * 
	 * @param str
	 *            中文字符串
	 * @param fill
	 *            分隔符
	 * @return 返回中文的拼音串
	 */
	public static String str2Pinyin(String str, String fill) {
		if (null == str) {
			return null;
		}
		try {
			StringBuilder sb = new StringBuilder();
			if (fill == null)
				fill = "";
			boolean isCn = true;
			for (int i = 0; i < str.length(); i++) {
				char c = str.charAt(i);
				if (i > 0 && isCn) {
					sb.append(fill);
				}
				if (c == ' ') {
					sb.append(fill);
				}
				// 1、判断c是不是中文
				if (c >= '\u4e00' && c <= '\u9fa5') {
					isCn = true;
					String[] piyins = PinyinHelper.toHanyuPinyinStringArray(c, format);
					if (null == piyins || 0 >= piyins.length) {
						continue;
					}
					sb.append(piyins[0]);
				} else {
					// 不是中文
					if (c >= 'A' && c <= 'Z') {
						sb.append((char)(c + 32));
					} else {
						sb.append(c);
					}
					isCn = false;
				}
			}
			return sb.toString();
		} catch (BadHanyuPinyinOutputFormatCombination e) {
//			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 拼音首字母
	 * 
	 * @param str
	 *            中文字符串
	 * @return 中文字符串的拼音首字母
	 */
	public static String strFirst2Pinyin(String str) {
		if (null == str) {
			return null;
		}
		try {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < str.length(); i++) {
				char c = str.charAt(i);
				// 1、判断c是不是中文
				if (c >= '\u4e00' && c <= '\u9fa5') {
					String[] piyins = PinyinHelper.toHanyuPinyinStringArray(c, format);
					if (null == piyins || 0 >= piyins.length) {
						continue;
					}
					sb.append(piyins[0].charAt(0));
				} else {
					// 不是中文
					if (c >= 'A' && c <= 'Z') {
						sb.append((char)(c + 32));
					} else {
						sb.append(c);
					}
				}
			}
			return sb.toString();
		} catch (BadHanyuPinyinOutputFormatCombination e) {
//			e.printStackTrace();
		}
		return null;
	}
}
