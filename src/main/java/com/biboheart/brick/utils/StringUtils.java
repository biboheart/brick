package com.biboheart.brick.utils;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
	/**
	 * 字符串前补"0"
	 * @param source 源字符串
	 * @param len 长度要求
	 * @return 前补"0"后的字符串
	 */
	public static String fillZeroFront(String source, int len) {
		if (null == source) {
			source = "";
		}
		if (len < source.length()) {
			return source;
		}
		int fillLen = len - source.length();
		StringBuilder sb = new StringBuilder(source);
		for (int i = 0; i < fillLen; i++) {
			sb.insert(0, "0");
		}
		return sb.toString();
	}

	/**
	 * 去除前导0
	 * @param source 源字符串
	 * @return 去除前导0后的字符串
	 */
	public static String removeZeroFront(String source) {
		if (null == source) {
			source = "";
		}
		int offset = 0;
		while (offset < source.length()) {
			char ch = source.charAt(offset);
			if (ch != '0') {
				break;
			}
			offset ++;
		}
		return source.substring(offset);
	}

	/**
	 * 取字符串开始的数字，遇到字母则结束
	 * @param str 源字符串
	 * @return 数字字符串
	 */
	public static String preserveNumbers(String str) {
		if (CheckUtils.isEmpty(str)) {
			return str;
		}
		StringBuilder sb = new StringBuilder();
		int offset = 0;
		while (offset < str.length()) {
			char ch = str.charAt(offset);
			if (ch < 48 || ch > 57) {
				break;
			}
			sb.append(ch);
			offset ++;
		}
		return sb.toString();
	}

	/**
	 * 将字符串超出给定长度的部分舍弃
	 * @param source 原字符
	 * @param len 长度要求
	 * @return 处理后的字符串
	 */
	public static String abandon(String source, int len) {
		if (null == source || source.length() <= len) {
			return source;
		}
		return source.substring(0, len);
	}

	/**
	 * 将字符串首字母转成小写
	 * @param source 源字符串
	 * @return 首字母小写的字符串
	 */
	public static String firstLowerCase(String source) {
		return source.substring(0, 1).toLowerCase() + source.substring(1);
	}

	/**
	 * 下划线格式字符串转换为驼峰格式字符串
	 * @param source 下划线格式字符串
	 * @return 驼峰格式字符串
	 */
	public static String underlineToCamel(String source) {
		if (source == null || source.trim().isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder(source);
        Matcher mc = Pattern.compile("_").matcher(source);
        int i = 0;
        while (mc.find()) {
            int position = mc.end() - (i++);
            sb.replace(position - 1, position + 1, sb.substring(position, position + 1).toUpperCase());
        }
        return sb.toString();
	}
	
	/**
	 * 驼峰格式字符串转换成下划线格式的字符串
	 * @param source 驼峰格式字符串
	 * @return 下划线格式字符串
	 */
	public static String camelToUnderline (String source) {
		if (source == null || source.trim().isEmpty()) {
            return "";
        }
		int len = source.length();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			char c = source.charAt(i);
			if (Character.isUpperCase(c)) {
                sb.append("_");
                sb.append(Character.toLowerCase(c));
            } else {
                sb.append(c);
            }
		}
        return sb.toString();
	}
	
	/**
	 * 生成随机字符串，包含随机字母与数字的组合
	 * @param count 字符串长度
	 * @return {count}位长的随机字符串
	 */
	public static String getRandomStr(int count) {
		return randomStr("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", count);
	}
	
	/**
	 * 生成随机数字字符串
	 * @param count 字符串长度
	 * @return {count}位长的随机字符串
	 */
	public static String getRandomNumberStr(int count) {
		return randomStr("0123456789", count);
	}

	/**
	 * 生成随机字母字符串,全小写字母的随机字符串
	 * @param count 字符串长度
	 * @return {count}位长的随机字符串
	 */
	public static String getRandomStringStr(int count) {
		return randomStr("abcdefghijklmnopqrstuvwxyz", count);
	}

	/**
	 * 生成随机字母字符串
	 * @param base 字符串基数，结果在base中取值
	 * @param count 字符串长度
	 * @return {count}位长的随机字符串
	 */
	private static String randomStr(String base, int count) {
		Random random = new Random();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < count; i++) {
			int number = random.nextInt(base.length());
			sb.append(base.charAt(number));
		}
		return sb.toString();
	}
	
	/**
	 * 生成随机姓名
	 * @return 姓名
	 */
	public static String getRandomNameStr() {
		Random random = new Random();
		String[] Surname = {"赵", "钱", "孙", "李", "周", "吴", "郑", "王", "冯", "陈", "褚", "卫", "蒋", "沈", "韩", "杨", "朱", "秦", "尤", "许",
                "何", "吕", "施", "张", "孔", "曹", "严", "华", "金", "魏", "陶", "姜", "戚", "谢", "邹", "喻", "柏", "水", "窦", "章", "云", "苏", "潘", "葛", "奚", "范", "彭", "郎",
                "鲁", "韦", "昌", "马", "苗", "凤", "花", "方", "俞", "任", "袁", "柳", "酆", "鲍", "史", "唐", "费", "廉", "岑", "薛", "雷", "贺", "倪", "汤", "滕", "殷",
                "罗", "毕", "郝", "邬", "安", "常", "乐", "于", "时", "傅", "皮", "卞", "齐", "康", "伍", "余", "元", "卜", "顾", "孟", "平", "黄", "和",
                "穆", "萧", "尹", "姚", "邵", "湛", "汪", "祁", "毛", "禹", "狄", "米", "贝", "明", "臧", "计", "伏", "成", "戴", "谈", "宋", "茅", "庞", "熊", "纪", "舒",
                "屈", "项", "祝", "董", "梁", "杜", "阮", "蓝", "闵", "席", "季"};
		String girl = "秀娟英华慧巧美娜静淑惠珠翠雅芝玉萍红娥玲芬芳燕彩春菊兰凤洁梅琳素云莲真环雪荣爱妹霞香月莺媛艳瑞凡佳嘉琼勤珍贞莉桂娣叶璧璐娅琦晶妍茜秋珊莎锦黛青倩婷姣婉娴瑾颖露瑶怡婵雁蓓纨仪荷丹蓉眉君琴蕊薇菁梦岚苑婕馨瑗琰韵融园艺咏卿聪澜纯毓悦昭冰爽琬茗羽希宁欣飘育滢馥筠柔竹霭凝晓欢霄枫芸菲寒伊亚宜可姬舒影荔枝思丽 ";
        String boy = "伟刚勇毅俊峰强军平保东文辉力明永健世广志义兴良海山仁波宁贵福生龙元全国胜学祥才发武新利清飞彬富顺信子杰涛昌成康星光天达安岩中茂进林有坚和彪博诚先敬震振壮会思群豪心邦承乐绍功松善厚庆磊民友裕河哲江超浩亮政谦亨奇固之轮翰朗伯宏言若鸣朋斌梁栋维启克伦翔旭鹏泽晨辰士以建家致树炎德行时泰盛雄琛钧冠策腾楠榕风航弘";
        int index = random.nextInt(Surname.length - 1);
        String name = Surname[index]; //获得一个随机的姓氏
        int i = random.nextInt(3);//可以根据这个数设置产生的男女比例
        if (i == 2) {
        	int j = random.nextInt(girl.length() - 2);
        	if (j % 2 == 0) {
        		name = name + girl.substring(j, j + 2);
        	} else {
        		name = name + girl.charAt(j);
        	}
        } else {
        	int j = random.nextInt(girl.length() - 2);
        	if (j % 2 == 0) {
        		name = name + boy.substring(j, j + 2);
        	} else {
        		name = name + boy.charAt(j);
        	}
        }
        return name;
	}
}
