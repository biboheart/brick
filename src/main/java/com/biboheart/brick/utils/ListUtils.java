package com.biboheart.brick.utils;

import com.biboheart.brick.adapter.TypeConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListUtils {
	/**
	 * 字符串转字符串列表
	 * @param source 源字符串
	 * @param regex 拆分字符，同String.split
	 * @return 转换后的列表
	 */
	public static List<String> string2ListString(String source, String regex) {
		if (null == source || source.isEmpty()) {
			return null;
		}
		if (null == regex || regex.isEmpty()) {
			regex = ",";
		}
		return new ArrayList<>(Arrays.asList(source.split(regex)));
	}

	/**
	 * 字符串转Integer列表
	 * @param source 源字符串
	 * @param regex 拆分字符，同String.split
	 * @return 转换后的列表
	 */
	public static List<Integer> string2ListInteger(String source, String regex) {
		if (null == source || source.isEmpty()) {
			return null;
		}
		if (null == regex || regex.isEmpty()) {
			regex = ",";
		}
		String[] strArr = source.split(regex);
		if (strArr.length == 0) {
			return null;
		}
		List<Integer> list = new ArrayList<>();
		for (String s : strArr) {
			try {
				list.add(Integer.parseInt(s));
			} catch (NumberFormatException ignore) {
			}
		}
		return list;
	}

	/**
	 * 字符串转Long列表
	 * @param source 源字符串
	 * @param regex 拆分字符，同String.split
	 * @return 转换后的列表
	 */
	public static List<Long> string2ListLong(String source, String regex) {
		if (null == source || source.isEmpty()) {
			return null;
		}
		if (null == regex || regex.isEmpty()) {
			regex = ",";
		}
		String[] strArr = source.split(regex);
		if (strArr.length == 0) {
			return null;
		}
		List<Long> list = new ArrayList<>();
		for (String s : strArr) {
			try {
				list.add(Long.parseLong(s));
			} catch (NumberFormatException ignore) {
			}
		}
		return list;
	}

	/**
	 * 字符串转列表
	 * @param source 源字符串
	 * @param regex	拆分字符，同String.split
	 * @param clazz	目标类型
	 * @return	转换后的列表
	 * @param <T>	泛型
	 */
	public static <T> List<T> string2List(String source, String regex, Class<T> clazz) {
		List<T> list = new ArrayList<>();
		if (null == source || source.isEmpty()) {
			return list;
		}
		if (null == regex || regex.isEmpty()) {
			regex = ",";
		}
		String[] strArr = source.split(regex);
		for (String s : strArr) {
			T val = TypeConverter.convert(s, clazz);
			if (null == val) {
				continue;
			}
			list.add(val);
		}
		return list;
	}

	/**
	 * 合并列表
	 * @param baseList 基础列表
	 * @param referList 被合并的列表，把这个列表中的各项合并到基础列表
	 * @param <N> 泛型对象
	 * @return 合并后的列表
	 */
	public static <N> List<N> mergeList(List<N> baseList, List<N> referList) {
		if (null == referList || referList.isEmpty()) {
			return baseList;
		}
		if (null == baseList) {
			baseList = new ArrayList<N>();
		}
		for (N o : referList) {
			if (!baseList.contains(o)) {
				baseList.add(o);
			}
		}
		return baseList;
	}

	/**
	 * 取交集
	 * 
	 * @param baseList 基础列表
	 * @param referList 比对的列表
	 * @param <N> 泛型
	 * @return 比对后在两列表中都存在的项组成新的列表
	 */
	public static <N> List<N> intersectionList(List<N> baseList, List<N> referList) {
		List<N> res = new ArrayList<N>();
		if (null == referList || referList.isEmpty()) {
			return res;
		}
		if (null == baseList || baseList.isEmpty()) {
			return res;
		}
		for (N o : referList) {
			if (baseList.contains(o) && !res.contains(o)) {
				res.add(o);
			}
		}
		return res;
	}
	
	/**
	 * 列表裁切，把referList列表中指定的数据从baseList中移除
	 * @param baseList baseList 原列表
	 * @param referList referList 参照列表
	 * @param <N> 泛型
	 * @return 移除数据后的列表
	 */
	public static <N> List<N> cutList(List<N> baseList, List<N> referList) {
		if (null == baseList || baseList.isEmpty() || null == referList || referList.isEmpty()) {
			return baseList;
		}
		for (N refer : referList) {
			baseList.remove(refer);
		}
		return baseList;
	}
}
