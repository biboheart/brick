package com.biboheart.brick.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.lang.Math.abs;

public final class ExchangeUtils {
    /**
     * 移动小数点
     * @param val 原值
     * @param moveLen 移动长度
     * @param fractionalLength 保留小数位
     * @return 移动后转的值
     */
    public static Double moveRadixPoint(String val, Integer moveLen, Integer fractionalLength) {
        if (null == val || val.isEmpty()) {
            return null;
        }
        BigDecimal valDecimal = new BigDecimal(val);
        if (moveLen == 0) {
            return valDecimal.doubleValue();
        }
        int type = moveLen < 0 ? 1 : 2; // 1左移，2右移
        BigDecimal moveDecimal = BigDecimal.valueOf(Math.pow(10, abs(moveLen)));
        if (type == 1) {
            valDecimal = valDecimal.divide(moveDecimal.abs(), fractionalLength, RoundingMode.HALF_UP);
        } else {
            valDecimal = valDecimal.multiply(moveDecimal.abs()).setScale(fractionalLength, RoundingMode.HALF_UP);
        }
        return valDecimal.doubleValue();
    }
}
