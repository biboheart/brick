package com.biboheart.brick.utils;

public class BitUtils {
    /**
     * 设置位值
     * @param flags 数值
     * @param index 二进制位下标
     * @param value 设置的值，0为假，大于0为真
     * @return 更新后的值
     */
    public static long set(Long flags, int index, int value) {
        if (null == flags) {
            flags = 0L;
        }
        if (value > 0) {
            flags |= (1L << index);
        } else {
            flags &= ~(1L << index);
        }
        return flags;
    }

    /**
     * 设置位值
     * @param flags 数值
     * @param index 二进制位下标
     * @param value 设置的值，0为假，大于0为真
     * @return 更新后的值
     */
    public static int set(Integer flags, int index, int value) {
        if (null == flags) {
            flags = 0;
        }
        if (value > 0) {
            flags |= (1 << index);
        } else {
            flags &= ~(1 << index);
        }
        return flags;
    }

    /**
     * 判断index位值的真假
     * @param flags 值
     * @param index 二进制位下标
     * @param value 判真还是判假，如果0判假，大于0判真
     * @return 对应真假结果
     */
    public static boolean is(Long flags, int index, int value) {
        if (null == flags) {
            flags = 0L;
        }
        if (index < 0) {
            return false;
        }
        if (value > 0) {
            return (flags&(1L<<index)) > 0;
        } else {
            return (~flags&(1L<<index)) > 0;
        }
    }

    /**
     * 判断index位值的真假
     * @param flags 值
     * @param index 二进制位下标
     * @param value 判真还是判假，如果0判假，大于0判真
     * @return 对应真假结果
     */
    public static boolean is(Integer flags, int index, int value) {
        if (null == flags) {
            flags = 0;
        }
        if (index < 0) {
            return false;
        }
        if (value > 0) {
            return (flags&(1L<<index)) > 0;
        } else {
            return (~flags&(1L<<index)) > 0;
        }
    }

    /**
     * 判断index位值的是否为真
     * @param flags 值
     * @param index 二进制位下标
     * @return 是否为真
     */
    public static boolean isTrue(Long flags, int index) {
        return is(flags, index, 1);
    }

    /**
     * 判断index位值的是否为真
     * @param flags 值
     * @param index 二进制位下标
     * @return 是否为真
     */
    public static boolean isTrue(Integer flags, int index) {
        return is(flags, index, 1);
    }

    /**
     * 判断index位值的是否为假
     * @param flags 值
     * @param index 二进制位下标
     * @return 是否为真
     */
    public static boolean isFalse(Long flags, int index) {
        return is(flags, index, 0);
    }
    /**
     * 判断index位值的是否为假
     * @param flags 值
     * @param index 二进制位下标
     * @return 是否为真
     */
    public static boolean isFalse(Integer flags, int index) {
        return is(flags, index, 0);
    }
}
